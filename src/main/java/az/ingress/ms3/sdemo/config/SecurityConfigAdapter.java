package az.ingress.ms3.sdemo.config;

import az.ingress.ms3.sdemo.config.jwt.AuthFilterConfigurerAdapter;
import az.ingress.ms3.sdemo.config.jwt.AuthService;
import az.ingress.ms3.sdemo.services.MyUserDetailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    private final SecurityProperties securityProperties;

    private final List<AuthService> authServices;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors().configurationSource(corsConfigurationSource());
        http.authorizeRequests()
                .antMatchers("/public**")
                .permitAll()
                .antMatchers("/authenticate**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/servlet**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/hello**")
                .permitAll()
                .antMatchers("/user**")
                .hasAnyRole("USER", "ADMIN")
                .antMatchers("/admin**")
                .hasAnyRole("ADMIN");

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.apply(new AuthFilterConfigurerAdapter(authServices));
        super.configure(http);
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("obama").password("{noop}123").roles("ADMIN");
        //auth.inMemoryAuthentication().withUser("kamil").password("123").roles("USER");
        //log.info("Injecting user details service {}", userDetailsService);
        // auth.userDetailsService(userDetailsService);
    }


    protected CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = securityProperties.getCors();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
