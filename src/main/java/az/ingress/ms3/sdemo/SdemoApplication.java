package az.ingress.ms3.sdemo;

import az.ingress.ms3.sdemo.domain.User;
import az.ingress.ms3.sdemo.jwt.JwtService;
import az.ingress.ms3.sdemo.repository.UserRepository;
import az.ingress.ms3.sdemo.services.MyUserDetailService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class SdemoApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final MyUserDetailService myUserDetailService;
    private final JwtService jwtService;
    private final BCryptPasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SdemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setPassword(passwordEncoder.encode("1234"));
        user.setUsername("admin");
        userRepository.save(user);

        Claims claims = jwtService.parseToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTYxMjYxMjQ0OSwiZXhwIjoxNjEyNjk4ODQ5LCJyb2xlcyI6WyJST0xFX0FETUlOIl19.2ZVTfKGbBvvWW0XQyNOheTgOwFdHM-G6u9lBJItQBqZpAj64-YetX1iUurxnXhh8WQTPky0o1ekcngxcjHgViQ");
        System.out.println(claims);

    }
}
