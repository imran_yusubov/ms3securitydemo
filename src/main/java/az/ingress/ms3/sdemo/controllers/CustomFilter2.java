package az.ingress.ms3.sdemo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
@Component
@Order(2)
public class CustomFilter2 implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.trace("Custom INGRESS 2 filter created {}",filterConfig);

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.trace("INGRESS FILTER 2 request source {}",request.getRemoteAddr());
      //  log.trace("INGRESS FILTER request source {}",request);
       // chain.doFilter(request,response);

        if (request instanceof HttpServletRequest) {
            String url = ((HttpServletRequest)request).getRequestURL().toString();
            String queryString = ((HttpServletRequest)request).getQueryString();

            log.trace(url);
            log.trace(queryString);
        }
        chain.doFilter(request,response);

    }

    @Override
    public void destroy() {
        log.trace("Custom INGRESS 2 filter destroyed");
    }
}
