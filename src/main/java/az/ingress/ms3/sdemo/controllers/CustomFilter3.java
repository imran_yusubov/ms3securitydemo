package az.ingress.ms3.sdemo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

@Slf4j
@Component
@Order(3)
public class CustomFilter3 implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.trace("Custom INGRESS 3 filter created {}",filterConfig);

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.trace("INGRESS FILTER 3 request source {}",request.getRemoteAddr());
      //  log.trace("INGRESS FILTER request source {}",request);
        chain.doFilter(request,response);

    }

    @Override
    public void destroy() {
        log.trace("Custom INGRESS 3 filter destroyed");
    }
}
