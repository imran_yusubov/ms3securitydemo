package az.ingress.ms3.sdemo.dto;

public class PasswordConstants {
    public static final int PASSWORD_MIN_LENGTH = 3;
    public static final int PASSWORD_MAX_LENGTH = 16;
}
