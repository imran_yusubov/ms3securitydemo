package az.ingress.ms3.sdemo.services;

import az.ingress.ms3.sdemo.domain.User;
import az.ingress.ms3.sdemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.jaxb.SpringDataJaxb;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MyUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Loading user {}", username);
        return userRepository.findByUsername(username)
                .map(user -> new MyUserPrincipal(user)).orElseThrow(() -> new UsernameNotFoundException(username));
    }


    @Transactional
    public void createUser(User user, long timeOut) {
        log.info("Creating user {} timeout {}", user, timeOut);
        userRepository.save(user);
        try {
            Thread.sleep(timeOut);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("Completed {} {}", user, timeOut);
    }


}
